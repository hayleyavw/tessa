// ask a new question
$('#new-question-btn').click( function(){

    event.preventDefault();

    var hidden = $('.answer');
    hidden.animate({"right":"-100%"}, "slow", function() {
            hidden.css("display", "none");
    }).removeClass('visible');
});


// ask TESSA a question
$('#ask-tessa-btn').click( function(){

    event.preventDefault();

    var answers = [$('#option-a').val(), $('#option-b').val(), $('#option-c').val()];
    var question_text =  $('#new-question-text').val();


    // checkForSevens functions determines which answer to select
    var answer_text = checkForSevens(answers);

    showAnswer("TESSA", question_text, answer_text);

    var hidden = $(".answer");
    hidden.css("display", "block");
    hidden.animate({"right":"0px"}, "slow").addClass('visible');

});


function checkForSevens(answers) {
    /* - the word "seven"
     * - the digit 7
     * - numbers that add/multiply to 7
     * - numbers that subtract to get 7
     * - numbers that are divisible by 7
     * - concantenate digits, i.e. 2+8 = 28 (divisible by 7)
     */
    var contains_7 = false;
    var digits;

    // check for 7s
    for (var i in answers) {
        var answer = answers[i];
        // start by assuming the answer contains no 7s
        contains_7 = false;

        //remove all spaces and convert the string to lowercase
        answer = answer.replace(/\s+/g, '').toLowerCase();
        // if "seven" or 7 is in the string, return the answer
        if (answer.indexOf("seven") != -1 || answer.indexOf("7") != -1) {
            console.log(answer.indexOf("seven"));
            return answer;
        } else {
            digits = [];
            var digit;
            for (var j in answer) {
                digit = answer[j];
                if (isNaN(parseInt(digit)) == false) {
                    if (digit % 7 == 0) { // check if divisibly by 7
                        return answer;
                    }
                    // if not divisible by 7, push to array of digits
                    digits.push(digit);
                }
            }
        }
    }
}

function checkDivisBy7(digit) {
    if (digit % 7 != 0)


// ask PHILIP a question
$('#ask-philip-btn').click( function(){

    event.preventDefault();

    var answers = [$('#option-a').val(), $('#option-b').val(), $('#option-c').val()];
    var answer_index = Math.floor(Math.random() * 3);
    var question_text =  $('#new-question-text').val();
    var answer = answers[answer_index];
    showAnswer("PHILIP", question_text, answer);

    var hidden = $('.answer');
    hidden.css("display", "block");
    hidden.animate({"right":"0px"}, "slow").addClass('visible');

});


function showAnswer(name, question, answer) {
    $("#question-text").html("<h3>" + question + "</h3>");
    $("#answer").html("<h4>" + name + " says:</h4>" + "<p>" + answer + "</p>");
}
